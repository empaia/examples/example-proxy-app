#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "No arguments provided. Please specify the app to execute, e.g. prostate or breast"
    exit 1
fi

VERSION=$(poetry version --short)
DOCKER_IMAGE_NAME="proxy-app-$1:$VERSION"

mkdir -p deployments
mkdir -p "deployments/$1"

# create app image

docker build --build-arg="SUPPORTED_TISSUE=$1" --tag="$DOCKER_IMAGE_NAME" .
docker save "$DOCKER_IMAGE_NAME" | gzip > "deployments/$1/app-image.tar.gz"

# copy ead
cp "app_metadata/$1/ead.json" "deployments/$1/"

# create app ui bundle
cd app_ui || exit
APP_NAME="${1^}" quasar build
cd dist/spa/ || exit
zip -r "../../../deployments/$1/app_ui_bundle.zip" .
cd ../../../

# copy app ui config
cp "eats_inputs/$1/configuration/app-ui-config-file.json" "deployments/$1/"


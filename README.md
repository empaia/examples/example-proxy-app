# Example Proxy App

The Example Proxy App is an example of a connector between an EMPAIA compatible environment and another external platform.

## Test setup

### Install EMPAIA App Test Suite (EATS)

```shell
# create virtual environment (recommended)
python3 -m venv .venv
source .venv/bin/activate

# install with pip
pip install empaia-app-test-suite
```

### Setup the test data

Create a `wsi-mount-points.json` with your test WSI data path and state it as parameter when starting the services with the eats command

```shell
eats services up ./wsi-mount-points.json --app-ui-disable-csp
```

`wsi-mount-points.json` content

```json
{
  "/folder/containing/slide/": "/data"
}
```

### Test the Example Proxy App with EATS

Start building the app (e.g. prostate) with

```shell
export app_name=prostate
docker build --build-arg="SUPPORTED_TISSUE=$app_name" --tag="proxy-app-$app_name"
```

Register the app with EATS by running

```shell
eats apps register "app_metadata/$app_name/ead.json" "proxy-app-$app_name" --customer-config-file ""./eats_inputs/$app_name/configuration/customer_configuration.json" --app-ui-url http://host.docker.internal:4200  --app-ui-config-file ""./eats_inputs/$app_name/configuration/app-ui-config-file.json" > "app-$app_name.env"
```

Check if the app is registered with

```shell
eats apps list
```

Register a preprocessing job

```shell
export $(xargs < "app-$app_name.env")
eats jobs register $APP_ID "eats_inputs/$1" --job-mode preprocessing > "job-$app_name.env"
```

Run the job with

```shell
eats jobs run "job-$app_name.env"
```

and wait for it to complete

```shell
export $(xargs < job.env)
eats jobs wait $EMPAIA_JOB_ID
```

Check job results

```shell
eats jobs inspect $EMPAIA_JOB_ID
docker logs $EMPAIA_JOB_ID
```

All of the above commands can also be run conviently using the `./run_app_creation.sh` and `./run_job.sh` scripts, e.g. by running the following command

```shell
./run_app_creation.sh prostate
./run_job.sh prostate
```

or

```shell
./run_app_creation.sh breast
./run_job.sh breast
```

Finally, export and show the results view:

```shell
eats jobs export $EMPAIA_JOB_ID ./job_outputs
cat job_outputs/view_url.json
```

Click or copy URL in value.

## UI Development

E.g. for Prostate app

```shell
yarn global add @quasar/cli
cd app_ui
yarn install
APP_NAME=Prostate quasar dev
```

or instead of `quasar dev` use

```shell
APP_NAME=Prostate quasar build
quasar serve dist/spa/ --port 4200
```

## Deployment

It easy to create all files needed for a deployment running, e.g. for prostate

```shell
./run_create_deployment.sh prostate
```

Afterwards all contents needed for deployment can be found at `deployments/prosate/`. Only thing missing is the `customer_configuration.json` which is not part of this repository.

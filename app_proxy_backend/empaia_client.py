import traceback
from pathlib import Path
from uuid import uuid4
from zipfile import ZipFile

import aiohttp
import requests

from .logging_tools import get_logger


class EmpaiaApiClient:
    class EmpaiaApiError(Exception):
        pass

    def __init__(self, app_api_url, job_id, headers, log_level):
        self._base_url = app_api_url
        self._job_id = job_id
        self._headers = headers
        self._logger = get_logger(__name__, log_level)

    def get_app_configuration(self):
        try:
            response = requests.get(f"{self._base_url}/v3/{self._job_id}/configuration", headers=self._headers)
            response.raise_for_status()
            return response.json()
        except Exception as exc:
            self._logger.error(str(exc))
            raise EmpaiaApiClient.EmpaiaApiError(str(exc)) from exc

    def get_slide_info_with_case_id(self):
        slide_info = self._api_request("GET", f"{self._base_url}/v3/{self._job_id}/inputs/slide", headers=self._headers)
        case_id_object = self._api_request("GET", f"{self._base_url}/v3/{self._job_id}/case", headers=self._headers)
        slide_info.update(case_id=case_id_object["id"])
        return slide_info

    async def download_slide(self, slide_id, target_dir):
        try:
            zip_filename = Path(target_dir) / f"{str(uuid4())}.zip"
            zip_url = f"{self._base_url}/v3/{self._job_id}/slides/{slide_id}/download"
            async with aiohttp.ClientSession() as session:
                async with session.get(zip_url, headers=self._headers) as response:
                    with open(zip_filename, "wb") as file:
                        async for data in response.content.iter_any():
                            file.write(data)
            self._extract_zip_file(zip_filename, target_dir)
            zip_filename.unlink()
        except Exception as exc:
            self._logger.error(str(exc))
            raise EmpaiaApiClient.EmpaiaApiError(str(exc)) from exc

    def finalize_job(self, view_url, slide_id):
        output_url = f"{self._base_url}/v3/{self._job_id}/outputs/view_url"
        data = {
            "name": "View URL",
            "creator_id": self._job_id,
            "creator_type": "job",
            "reference_id": slide_id,
            "reference_type": "wsi",
            "type": "string",
            "value": view_url,
        }
        self._api_request("POST", output_url, headers=self._headers, json=data)
        finalize_url = f"{self._base_url}/v3/{self._job_id}/finalize"
        self._api_request("PUT", finalize_url, headers=self._headers)

    def job_failure(self, error_message):
        metadata = {"user_message": error_message}
        failure_url = f"{self._base_url}/v3/{self._job_id}/failure"
        self._api_request("PUT", failure_url, json=metadata, headers=self._headers)

    def _extract_zip_file(self, zip_filename, target_dir):
        with ZipFile(zip_filename, "r") as zip_ref:
            zip_ref.extractall(path=target_dir)

    def _api_request(self, method, url, **kwargs):
        try:
            response = requests.request(method, url, **kwargs)
            response.raise_for_status()
            return response.json()
        except Exception as exc:
            self._logger.error(str(exc))
            self._logger.error(f"{exc}: {traceback.format_exc()}")
            raise EmpaiaApiClient.EmpaiaApiError(str(exc)) from exc

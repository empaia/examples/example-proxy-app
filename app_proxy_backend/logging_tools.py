import logging
import sys

FORMATTER = logging.Formatter("%(asctime)s | %(name)s | %(levelname)s | %(message)s")


def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_logger(logger_name, log_verbosity):
    log_level = max(logging.WARN - 10 * log_verbosity, logging.DEBUG)
    logger = logging.getLogger(logger_name)
    logger.setLevel(log_level)
    logger.addHandler(get_console_handler())
    logger.propagate = False
    return logger

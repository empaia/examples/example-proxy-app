import argparse
import asyncio
import os
import sys
import tempfile
import time

from .empaia_client import EmpaiaApiClient
from .external_client import ExternalApiClient

SUPPORTED_TISSUE = os.environ["SUPPORTED_TISSUE"]
APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

POLL_DELAY_SECS = 5
PROCESSING_TIMEOUT_SECS = 3600 * 4  # wait max. 4 hours


async def main(log_verbosity: int):
    try:
        empaia_client = EmpaiaApiClient(APP_API, JOB_ID, HEADERS, log_verbosity)
        app_configuration = empaia_client.get_app_configuration()
        external_client = ExternalApiClient(app_configuration["customer"], log_verbosity)

        slide_info = empaia_client.get_slide_info_with_case_id()

        if slide_info["stain"].upper() not in ["HE", "H_AND_E"]:
            raise ExternalApiClient.ExternalApiError(
                f"Processing could not be started due to unsupported stain {slide_info['stain']}. Only HE (H_AND_E) is supported."
            )
        else:
            slide_info["stain"] = "HE"
        if SUPPORTED_TISSUE.upper() != slide_info["tissue"].upper():
            raise ExternalApiClient.ExternalApiError(
                f"Processing could not be started due to unsupported tissue {slide_info['tissue']}. Only {SUPPORTED_TISSUE} is supported."
            )

        with tempfile.TemporaryDirectory() as temp_dir:
            await empaia_client.download_slide(slide_info["id"], temp_dir)
            wsi_id = external_client.upload_slide_and_trigger_processing(slide_info, temp_dir)

        waiting_start = time.time()
        view_url = external_client.get_view_url(wsi_id)
        while not view_url:
            if time.time() - waiting_start > PROCESSING_TIMEOUT_SECS:
                raise ExternalApiClient.ExternalApiError(
                    f"Processing did not finish within timeout of {PROCESSING_TIMEOUT_SECS} seconds"
                )
            await asyncio.sleep(POLL_DELAY_SECS)
            view_url = external_client.get_view_url(wsi_id)
        empaia_client.finalize_job(view_url, slide_info["id"])

    except ExternalApiClient.ExternalApiError as exc:
        empaia_client.job_failure(str(exc))
    except EmpaiaApiClient.EmpaiaApiError as exc:
        sys.exit(1)


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="increase logging verbosity", action="count", default=0)
args = parser.parse_args()

loop = asyncio.get_event_loop()
loop.run_until_complete(main(log_verbosity=args.verbose))

from .logging_tools import get_logger


class ExternalApiClient:
    class ExternalApiError(Exception):
        pass

    def __init__(self, customer_configuration, log_level: int):
        self._logger = get_logger(__name__, log_level)
        self._logger.info("Initialize client with configuration")
        pass

    def upload_slide_and_trigger_processing(self, slide_info, source_dir):
        # TODO: upload/register slide and trigger processing
        return "dummy_id"

    def get_view_url(self, wsi_id):
        # TODO: get URL to case/slide/etc
        return f"https://test.empaia.org/{wsi_id}/"  # dummy url

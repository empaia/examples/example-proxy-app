#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "No arguments provided. Please specify the app to execute, e.g. prostate or breast"
    exit 1
fi

export $(xargs < "app-$1.env")
eats jobs register $APP_ID "eats_inputs/$1" --job-mode preprocessing > "job-$1.env"
eats jobs run "job-$1.env"
export $(xargs < "job-$1.env")                                             
eats jobs wait $EMPAIA_JOB_ID
eats jobs inspect $EMPAIA_JOB_ID
docker logs $EMPAIA_JOB_ID

#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "No arguments provided. Please specify the app to execute, e.g. prostate or breast"
    exit 1
fi

VERSION=$(poetry version --short)
DOCKER_IMAGE_NAME="proxy-$1:$VERSION"

docker build --build-arg="SUPPORTED_TISSUE=$1" --tag="$DOCKER_IMAGE_NAME" .
eats apps register "app_metadata/$1/ead.json" "$DOCKER_IMAGE_NAME" --customer-config-file "./eats_inputs/$1/configuration/customer_configuration.json" --app-ui-url http://host.docker.internal:4200 --app-ui-config-file "./eats_inputs/$1/configuration/app-ui-config-file.json" > "app-$1.env"

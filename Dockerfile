FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.1.59@sha256:4a9ff4c3dc5bf77f00af84844ca044455b5e4e023eaa12c47c1f03f7e68d8711 AS builder
COPY . /app_proxy_backend
WORKDIR /app_proxy_backend
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.1.58@sha256:88585c0b11134ab90ec6838908f2fb4472abf557bcb14929f84f3927b08216c7
ARG SUPPORTED_TISSUE
ENV SUPPORTED_TISSUE=$SUPPORTED_TISSUE
COPY --from=builder /app_proxy_backend/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /app_proxy_backend/dist /artifacts
RUN pip install /artifacts/*.whl

ENV PYTHONUNBUFFERED=0

ENTRYPOINT ["python", "-m", "app_proxy_backend"]
CMD ["-v"]
